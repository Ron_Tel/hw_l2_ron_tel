#include <iostream>
#include <string>
#include <algorithm>
#include <map>

#include "Nucleus.h"


using std::string;

void Gene::init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand) 
{
	this->_start = start;
	this->_end = end;
	this->_strand_dna_complementary_on = on_complementary_dna_strand;
}

unsigned int Gene::get_start (void) const 
{
	return (this->_start);
}

unsigned int Gene::get_end(void) const
{
	return (this->_end);
}

bool Gene:: is_on_complementary_dna_strand (void) const
{
	return (this->_strand_dna_complementary_on);
}

void Gene::set_start(unsigned int newStart) 
{
	this->_start = newStart;
}

void Gene::set_end(unsigned int  newEnd) 
{
	this->_end = newEnd;
}

void Gene::SetOnCompletDna (bool newCompDna)
{
	this->_strand_dna_complementary_on = newCompDna;
}

void Nucleus:: init(const string dna_sequence) 
{
	if (dna_sequence.find_first_not_of("AGCT") != string::npos) //if illigle dna
	{
		std::cerr << "dna sequence is illigle";
		_exit(1);
	}
	this->_DNA_strand = dna_sequence;
	this->_complementary_DNA_strand = dna_sequence;

	std::map <char, char> MAP({ {'G' , 'c'} , {'C' , 'g'} , {'A' , 't'} , {'T' , 'a'} });  //map of possible replaces
	for (auto i : MAP)
		std::replace(this->_complementary_DNA_strand.begin(), this->_complementary_DNA_strand.end(), i.first , i.second);

	std::transform(_complementary_DNA_strand.begin(), _complementary_DNA_strand.end(), _complementary_DNA_strand.begin(), ::toupper); //transform string into uppercase

}

string Nucleus :: get_RNA_transcript(const Gene& gene) const 
{
	string rna = gene.is_on_complementary_dna_strand() ? this->_complementary_DNA_strand.substr(gene.get_start() , gene.get_end() - gene.get_start() + 1 ) : this->_DNA_strand.substr(gene.get_start()  , gene.get_end() - gene.get_start() + 1); //cut the dna sequence to rna
	std::replace(rna.begin(), rna.end(), 'T', 'U');
	return rna;
}

string Nucleus :: get_reversed_DNA_strand() const 
{
	string temp = this->_DNA_strand;
	string toRet = "";
	for (int i = temp.length() - 1; i >= 0; i--)
		toRet += temp[i];
	return toRet;
}

unsigned int Nucleus::get_num_of_codon_appearances(const string& codon) const
{
	unsigned int occurrences = 0;
	string::size_type start = 0;

	while ((start = this->_DNA_strand.find(codon, start)) != string::npos) {
		occurrences++;
		start += codon.length();
	}
	return occurrences;
}

string Nucleus ::get_DNA_strand () //getter for bonus
{
	return (this->_DNA_strand);
}


