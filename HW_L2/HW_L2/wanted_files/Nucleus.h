#pragma once
#include <iostream>

using std::string;


class Gene 
{
	unsigned int _start;
	unsigned int _end;
	bool _strand_dna_complementary_on;

	public:
		void init(const unsigned int start, const unsigned int end, const bool on_complementary_dna_strand);
		unsigned int get_start (void) const;
		unsigned int get_end (void) const;
		bool is_on_complementary_dna_strand (void) const;
		void set_start(unsigned int newStart);
		void set_end (unsigned int newEnd);
		void SetOnCompletDna(bool newCompDna);
};

class Nucleus 
{
	string _DNA_strand;
	string _complementary_DNA_strand;


	public:
		void init(const string dna_sequence);
		string get_RNA_transcript(const Gene& gene) const;
		string get_reversed_DNA_strand() const;
		unsigned int get_num_of_codon_appearances(const std::string& codon) const;
		string get_DNA_strand(); //for the bounus
};

