#include <iostream>
#include <string>
#include <algorithm>
#include "Cell.h"
#include "Nucleus.h"
#include "Virus.h"

using std::string;

void Virus :: init(string RNA_sequence) 
{
	this->_RNA_sequence = RNA_sequence;
}

void Virus :: infect_cell (Cell &Cell) 
{
	Nucleus Nucopy = Cell.get_Nucleus();
	string cell_dna = Nucopy.get_DNA_strand();
	string infected_dna = "";
	infected_dna += cell_dna.substr(0, cell_dna.length() / 2);
	infected_dna += this->_RNA_sequence; //adding virus rna in the middel of the dna
	infected_dna += cell_dna.substr(cell_dna.length() / 2 , cell_dna.length() - cell_dna.length() / 2);
	std::replace(infected_dna.begin(), infected_dna.end(), 'U', 'T');
	Cell.init(infected_dna, Cell.get_gene()); //initial the cell with infected dna
}