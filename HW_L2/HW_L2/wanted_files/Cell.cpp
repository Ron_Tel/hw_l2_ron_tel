#include <iostream>
#include "Ribosome.h"
#include "Mitochondrion.h"
#include "Nucleus.h"
#include "Protein.h"
#include "Cell.h"

using std::string;

void Cell :: init(const string dna_sequence, const Gene glucose_receptor_gene) 
{
	this->_glucose_receptor_gene = glucose_receptor_gene;
	this->_mitochondrion.init();
	this->_nucleus.init(dna_sequence);
}

bool Cell::get_ATP()
{
	string rna = this->_nucleus.get_RNA_transcript(this->_glucose_receptor_gene);
	Protein* protein = this->_ribosome.create_protein(rna);
	if (!protein) 
	{
		std::cerr << "cant produce protein";
		_exit(1);
	}
	this->_mitochondrion.insert_glucose_receptor(*protein);
	this->_mitochondrion.set_glucose(50);
	if (this->_mitochondrion.produceATP())
		return true;
	return false;
}

Nucleus Cell :: get_Nucleus () 
{
	return (this->_nucleus);
}

Gene Cell ::get_gene () 
{
	return (this->_glucose_receptor_gene);
}