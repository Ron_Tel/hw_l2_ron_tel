#pragma once

#include <iostream>
#include "Cell.h"

using std::string;

class Virus 
{
	string _RNA_sequence;
	public:
		void init(string RNA_sequence);
		void infect_cell(Cell& Cell);
};